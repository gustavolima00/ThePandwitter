class Profile < ApplicationRecord
  belongs_to :user
  has_many :post, dependent: :destroy

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "Icon.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

end
