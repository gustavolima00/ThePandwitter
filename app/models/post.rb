class Post < ApplicationRecord
  belongs_to :profile
  has_attached_file :image, styles: { medium: "300x300>", normal: "700x700>" }, default_url: "Pandwitter.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

end
