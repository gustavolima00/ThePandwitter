# README

EP3 Orientação a Objetos - Unb Campus cama
Gustavo Marques Lima - 17/0035158
## Requisitos
*ImageMagic*
  O image magic deve estar instalado na máquina, usando o comando:
  ``sudo apt-get install imagemagick -y``

## Instalação

1. Faça o clone deste projeto com ```$ git clone https://gitlab.com/gustavolima00/Pandwitter.git ```
2. Acesse a pasta do projeto via terminal
3. Execute os seguintes comandos ``` $ bundle install ``` ``` $ rake db:drop ``` ``` $ rake db:create ``` ``` $ rake db:migrate ``` ``` $ rake db:seed ```
4. Rode a aplicação com ``` $ rails s ```
5. Vá ao seu navegador e acesse o link pelo caminho gerado, por padrão o link gerado é "localhost:3000"

## Funcionamento

  **Geral**
  O Funcionamento do site é bastante simples, ao entrar no site a tela inicil pede o login do usuário, o que é necessário para fazer postagens. Porém é possível ver postagens sem fazer login

  **Permissoes**
  As permissoes do site consistem em postagem, edição, remoção de posts e profiles. Um usuário adim pode fazer todas essas coisas, um usuário normal não pode detetar nem acessar os profiles
  ` /profiles ` porém, pode deletar e editar suas postagens. Um usuário não logado pode apenas ver as postagens na pagina `/posts`

  **Usuário admin**
  Por padrão as informações do usuário admin são:
  ` email:"admin@admin.admin"
    password:"adminadmin"`
